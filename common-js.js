// (function(exports) {
// 	var weekdays = [
// "sunday", "monday","tuesday", "wednesday", "thursday", "friday"
// ];
// 	exports.dayNumber = function(day) {
// 		return weekdays.indexOf(day);
// 	};
// 	exports.dayName = function(num) {
// 		return weekdays[num];
// 	};
// })(this.weekday = {})
// var Weekdays = this.weekday;
// This is exporting the interface directly
// (function(exports) {
// 	exports.Months = function() {
// 		var months = [
// "jan", "feb", "mar","apr","may","jun","jul","aug","sep","oct","nov","dec"
// ];
// 		return {
// 			monthName: function(num) {
// 				return months[num];
// 			},
// 			monthNumber: function(name) {
// 				return months.indexOf(name);
// 			}
// 		};
// 	};
// })(this.imports = {})
// var month = new imports.Months();
// This exports the constructor to the imports object

// function CodeEvaluator(codeString) {
// 	eval(codeString);
// 	return exports;
// }
// var codeString = "(function(exports){ exports.adder = function(a,b){" +
// "return a+b}})(this.exports = {})";
// var exports = CodeEvaluator(codeString);
// Barebones code evaluator returns the exports object

// function CodeEvaluator(codeString) {
// 	if(codeString in CodeCache) {
// 		return CodeCache[codeString];
// 	} else {
// 		var code = new Function("exports", codeString);
// 		var exports = {};
// 		code(exports);
// 		CodeCache[codeString] = exports;
// 		return exports;
// 	}
// }

// var CodeCache = {};
// var codestr = "exports.adder = function(a,b){return a+b}";
// var exports = CodeEvaluator(codestr);
// console.log(exports.adder(2,3)); // 5
// Proper code evaluator which wraps the given function protecting scope.
// Cached implemntation of the CodeEvaluator (require) function in
// commonjs

function CodeEvaluator(codeString) {
	if(codeString in CodeCache) {
		return CodeCache[codeString];
	} else {
		var code = new Function("exports, module", codeString);
		var exports = {};
		var module = {exports: exports};
		code(exports, module);
		CodeCache[codeString] = module.exports;
		return module.exports;
	}
}

var CodeCache = {};
var codestr = "module.exports = function(a,b){return a+b}";
var exports = CodeEvaluator(codestr);
console.log(exports(2,3)); // 5
// Time to allow constructor exports
// Learnings:
// 1. You cannot attach a function to the exports object alone and
// which is why you need the module object to retain the value
// assigned to the exports.
// 2. Now you can attach individual functions to the exports object
// with specific keys or you can also make a module export a single
// constructor functions which you can use for making proper modules.
