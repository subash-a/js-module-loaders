var map = {
	entry: `
	var myAdder = CodeEvaluator(adderCodeString);
	exports.multiply = function(a,b) {
		var result = a;
		while(b > 1){
			result = myAdder.add(result, a);
			b --;
		}
		return result;
	};
	`,
	adderCodeString: `
	exports.add = function(a,b) { return a+b};
	`
};
function browserify(entrypoint) {
	// NOTE: Since we do not have name to pathname resolver we are using direct variable names
	// 1. Call CodeEvaluator where found
	// 2. Once exports are returned loop through them and convert to string
	// 3. assign the converted string to the variable in the codeString.
	var codeEvalRegexp = /CodeEvaluator\(([\w]+)\)/g;
	var functionRegexp = /"(function[\s]*\([\w,\s]+\)[\s]*\{[\w\+;\s]+\})"/g;
	while((res = codeEvalRegexp.exec(entrypoint)) !== null) {
		var exports = CodeEvaluator(map[res[1]]);
		var str = {};
		for(m in exports) {
			str[m] = exports[m].toString();
		}
		// NOTE: This is an extremely gross hack please do not follow this
		// technique everywhere I have used this only for demonstration purposes
		// and this should not be used for actual implementation of a browserify
		// kind of mechanism.
		var stringReplacement = JSON.stringify(str);
		while((freplace = functionRegexp.exec(stringReplacement)) !== null) {
			stringReplacement = stringReplacement.replace(freplace[0], freplace[1]);
		}
		entrypoint = entrypoint.replace(res[0], stringReplacement);
	}
	return CodeEvaluator(entrypoint);
}

var mul = browserify(map.entry);
console.log(mul.multiply(2,3));
